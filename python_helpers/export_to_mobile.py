# PyTorch
import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, random_split
from torch.utils.mobile_optimizer import optimize_for_mobile
import torchvision


def export_to_mobile(model, output_path):
    torchscript_model = torch.jit.script(model)
    torchscript_model_optimized = optimize_for_mobile(torchscript_model)
    torch.jit.save(torchscript_model_optimized, output_path)
    print(f'done exporting model to {output_path}')


if __name__ == "__main__":
    path = 'u2netp.model'
    model = torch.load(path)
    export_to_mobile(model, 'u2netp.mobile')
