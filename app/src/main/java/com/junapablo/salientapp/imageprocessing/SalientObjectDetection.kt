package com.junapablo.salientapp.imageprocessing

import android.content.Context
import android.graphics.Bitmap
import org.pytorch.IValue
import org.pytorch.Module
import org.pytorch.torchvision.TensorImageUtils

object SalientObjectDetection {
    lateinit var module: Module

    private const val IMAGE_SIZE = 224

    fun loadModule(name: String, context: Context)
    {
        module = Module.load(Utils.assetFilePath(context, name))
    }

    fun runInference(bitmap: Bitmap): Bitmap {
        if (bitmap.width != IMAGE_SIZE || bitmap.height != IMAGE_SIZE) {
            error("Incorrect image size!")
        }
        // this might be inconsistent with the original training?
        val inputTensor = TensorImageUtils.bitmapToFloat32Tensor(
            bitmap,
            TensorImageUtils.TORCHVISION_NORM_MEAN_RGB,
            TensorImageUtils.TORCHVISION_NORM_STD_RGB
        )
        val inputIValue = IValue.from(inputTensor)
        val outputValue = module.forward(inputIValue)
        val outputTuple = outputValue.toTuple()
        val outputTensor = outputTuple.first().toTensor()
        val floatArray = outputTensor.dataAsFloatArray;
        return Utils.floatArrayToMaskBitmap(floatArray, bitmap, IMAGE_SIZE, IMAGE_SIZE)
    }
}