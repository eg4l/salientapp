package com.junapablo.salientapp.imageprocessing

import android.R
import android.R.attr.bitmap
import android.R.id
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_EDIT
import android.graphics.*
import com.junapablo.salientapp.photoediting.EditImageActivity
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.nio.ByteBuffer


object Utils {
    fun assetFilePath(context: Context, assetName: String): String {
        val file = File(context.filesDir, assetName)
        if (file.exists() && file.length() > 0) {
            return file.absolutePath
        }
        context.assets.open(assetName).use { `is` ->
            FileOutputStream(file).use { os ->
                val buffer = ByteArray(4 * 1024)
                var read: Int
                while (`is`.read(buffer).also { read = it } != -1) {
                    os.write(buffer, 0, read)
                }
                os.flush()
            }
            return file.absolutePath
        }
    }

    fun floatArrayToMaskBitmap(
            floatArray: FloatArray,
            colorBitmap: Bitmap,
            width: Int,
            height: Int,
            reverseScale: Boolean = false
    ) : Bitmap {

        // Create empty bitmap in RGBA format (even though it says ARGB but channels are RGBA)
        val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val byteBuffer = ByteBuffer.allocate(width * height * 4)

        // mapping smallest value to 0 and largest value to 255
        val maxValue = floatArray.maxOrNull() ?: 1.0f
        val minValue = floatArray.minOrNull() ?: 0.0f
        val delta = maxValue-minValue
        var tempValue :Byte

        // Define if float min..max will be mapped to 0..255 or 255..0
        val conversion = when(reverseScale) {
            false -> { v: Float -> ((v - minValue) / delta * 255).toInt().toByte() }
            true -> { v: Float -> (255 - (v - minValue) / delta * 255).toInt().toByte() }
        }

        // copy each value from float array to RGB channels and set alpha channel
        floatArray.forEachIndexed { i, value ->
            tempValue = conversion(value)
            byteBuffer.put(4 * i + 3, tempValue)
        }

        var i = 0
        for(y in 0 until colorBitmap.height)
            for(x in 0 until colorBitmap.width) {
                val color = colorBitmap.getColor(x, y)
                val red = (color.red() * 255).toInt().toByte()
                val green = (color.green() * 255).toInt().toByte()
                val blue = (color.blue() * 255).toInt().toByte()

                byteBuffer.put(4 * i + 0, red)
                byteBuffer.put(4 * i + 1, green)
                byteBuffer.put(4 * i + 2, blue)

                i++
            }

        bmp.copyPixelsFromBuffer(byteBuffer)

        return bmp
    }

    fun loadBitmapFromAssets(context: Context, assetName: String): Bitmap{
        return BitmapFactory.decodeStream(File(context.filesDir, assetName).inputStream())
    }

    fun editPhoto(context: Context, inputName: String){
        val intent = Intent(context, EditImageActivity::class.java).apply {
            putExtra(EditImageActivity.BITMAP_INPUT_EXTRA_PATH, inputName)
            action = ACTION_EDIT
        }
        context.startActivity(intent)
    }
}