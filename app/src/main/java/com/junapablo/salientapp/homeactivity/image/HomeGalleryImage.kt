package com.junapablo.salientapp.homeactivity.image

import java.io.File

class HomeGalleryImage(
    val content: File,
    val previewContent: File,
    val imageName: String
)