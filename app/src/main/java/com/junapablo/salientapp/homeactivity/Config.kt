package com.junapablo.salientapp.homeactivity

class Config {
    companion object{
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_IMAGE_IMPORT = 2
        enum class HomeGalleryMode{
            ORIGINAL,
            PREVIEW
        }
        val homeGalleryViewMap : HashMap<Int, HomeGalleryMode>  = hashMapOf(
            1 to HomeGalleryMode.ORIGINAL,
            -1 to HomeGalleryMode.PREVIEW
        )
    }
}