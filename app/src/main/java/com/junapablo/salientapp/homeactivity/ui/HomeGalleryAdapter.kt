package com.junapablo.salientapp.homeactivity.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.junapablo.salientapp.R
import com.junapablo.salientapp.homeactivity.Config
import com.junapablo.salientapp.homeactivity.Config.Companion.homeGalleryViewMap
import com.junapablo.salientapp.homeactivity.HomeGalleryUtils
import com.junapablo.salientapp.homeactivity.image.HomeGalleryImage
import com.junapablo.salientapp.imageprocessing.Utils
import com.junapablo.salientapp.storage.*

class HomeGalleryAdapter(private val dataSet: ArrayList<HomeGalleryImage>) :
    RecyclerView.Adapter<HomeGalleryAdapter.ViewHolder>() {
    private var currentView = 1
    lateinit var context: Context

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.homeGalleryImageView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.activity_main_gallery_row, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.imageView.setOnClickListener {
            if (homeGalleryViewMap[currentView] == Config.Companion.HomeGalleryMode.ORIGINAL)
                showFullSize(position)
            else
                Utils.editPhoto(context, dataSet[position].imageName)
        }

        viewHolder.imageView.setOnLongClickListener {
            deleteAlert(position)
            //TODO else go to editor activity
            true
        }

        if (homeGalleryViewMap[currentView] == Config.Companion.HomeGalleryMode.ORIGINAL)
            viewHolder.imageView.setImageBitmap(HomeGalleryUtils.getSourceImageBitmap(dataSet[position]))
        else
            viewHolder.imageView.setImageBitmap(HomeGalleryUtils.getPreviewImageBitmap(dataSet[position]))
    }

    override fun getItemCount() = dataSet.size

    fun addImage(rawImage: Bitmap, preview: Bitmap) {
        try {
            val homeGalleryImage = StorageManager.saveImagesInMainGallery(rawImage, preview)
            dataSet.add(homeGalleryImage)
            notifyDataSetChanged()
        } catch(e: BitmapSaveFailure) {
            e.printStackTrace()
        }
    }

    fun changeView() {
        currentView *= -1
        notifyDataSetChanged()
    }

    private fun showFullSize(position: Int) {
        val dialog = Dialog(context, R.style.Theme_AppCompat_NoActionBar)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.full_size_image)
        val closeButton = dialog.findViewById<ImageButton>(R.id.homeCloseFullSizeButton)
        val fullImageView = dialog.findViewById<ImageView>(R.id.homeFullSizeImageView)
        fullImageView.setImageBitmap(HomeGalleryUtils.getSourceImageBitmap(dataSet[position]))
        closeButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun deleteAlert(adapterPosition: Int) {
        val adb = AlertDialog.Builder(context)
        adb.setTitle("Do you want to delete this photo?")
        adb.setIcon(R.drawable.close_icon)
        adb.setPositiveButton("Yes") { _: DialogInterface, _: Int ->
            deleteImage(adapterPosition)
        }
        adb.setNegativeButton("No") { _: DialogInterface, _: Int -> }
        adb.show()
    }

    private fun deleteImage(taskId: Int) {
        val imgName = dataSet[taskId].imageName
        dataSet.removeAt(taskId)
        StorageManager.deleteImageFromMainGallery(imgName)
        notifyItemRemoved(taskId)
    }
}