package com.junapablo.salientapp.homeactivity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.junapablo.salientapp.R
import com.junapablo.salientapp.homeactivity.image.HomeGalleryImage
import com.junapablo.salientapp.homeactivity.ui.HomeGalleryAdapter
import com.junapablo.salientapp.homeactivity.Config.Companion.REQUEST_IMAGE_CAPTURE
import com.junapablo.salientapp.homeactivity.Config.Companion.REQUEST_IMAGE_IMPORT
import com.junapablo.salientapp.imageprocessing.SalientObjectDetection
import com.junapablo.salientapp.storage.StorageManager
import java.io.FileNotFoundException
import java.util.stream.Collectors

class MainActivity : AppCompatActivity() {
    private var homeGalleryImages = ArrayList<HomeGalleryImage>()
    private lateinit var homeGalleryAdapter : HomeGalleryAdapter
    private lateinit var homeGallery : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)
        StorageManager.setup(this)
        fetchGalleryData()
        setupGallery()
        setupButtons()
        setupModel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //TAKE PHOTO
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val scaledImage = Bitmap.createScaledBitmap(imageBitmap, resources.getInteger(R.integer.image_width), resources.getInteger(R.integer.image_height), false)
            homeGalleryAdapter.addImage(scaledImage, makePreview(scaledImage))
        //IMPORT IMAGE
        } else if (requestCode == REQUEST_IMAGE_IMPORT && resultCode == RESULT_OK){
            try{
                val imageUri = data?.data
                val imageStream = imageUri?.let { contentResolver.openInputStream(it) }
                val imageBitmap = BitmapFactory.decodeStream(imageStream)
                val scaledImage = Bitmap.createScaledBitmap(imageBitmap, resources.getInteger(R.integer.image_width), resources.getInteger(R.integer.image_height), false)
                homeGalleryAdapter.addImage(scaledImage, makePreview(scaledImage))
            }
            catch (e : FileNotFoundException) {
                e.printStackTrace()
                Toast.makeText(this, "Image could not be read.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun fetchGalleryData() {
        homeGalleryImages = StorageManager
            .getMainGalleryFiles()
            .stream()
            .map {HomeGalleryImage(it.first, it.second, it.first.name)}
            .collect(Collectors.toList()) as ArrayList<HomeGalleryImage>
    }

    private fun setupGallery() {
        homeGalleryAdapter = HomeGalleryAdapter(homeGalleryImages)
        homeGallery = findViewById(R.id.homeGallery)
        homeGallery.layoutManager = LinearLayoutManager(this)
        homeGallery.adapter = homeGalleryAdapter
        homeGalleryAdapter.context = this
    }

    private fun setupButtons() {
        //Take photo
        findViewById<ImageButton>(R.id.homeTakePhotoButton).setOnClickListener {
            dispatchTakePictureIntent()
        }
        //Import photo
        findViewById<ImageButton>(R.id.homePickImageButton).setOnClickListener {
            dispatchImportPictureIntent()
        }
        //Change View
        findViewById<ImageButton>(R.id.homeDisplayChangeButton).setOnClickListener {
            homeGalleryAdapter.changeView()
        }
        //Full gallery
        findViewById<ImageButton>(R.id.homeToGalleryButton).setOnClickListener {
            openModifiedImageGallery()
        }

    }

    private fun setupModel() {
        SalientObjectDetection.loadModule("u2netp.mobile", this)
    }

    private fun makePreview(image : Bitmap) : Bitmap{
        return SalientObjectDetection.runInference(image)
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this,
                "Temporary problem with your camera. Please restart app and try again.",
                Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun dispatchImportPictureIntent() {
        val importPictureIntent = Intent(Intent.ACTION_PICK)
        importPictureIntent.type = "image/*"
        try {
            startActivityForResult(importPictureIntent, REQUEST_IMAGE_IMPORT)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this,
                "Temporary problem with browsing gallery. Please restart app and try again.",
                Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun openModifiedImageGallery(){
        //TODO
    }
}