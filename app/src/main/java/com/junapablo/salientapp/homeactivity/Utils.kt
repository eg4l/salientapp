package com.junapablo.salientapp.homeactivity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.junapablo.salientapp.homeactivity.image.HomeGalleryImage
import java.io.FileInputStream

class HomeGalleryUtils {
    companion object{
        fun getSourceImageBitmap(homeGalleryImage : HomeGalleryImage) : Bitmap {
            val fis = FileInputStream(homeGalleryImage.content)
            return BitmapFactory.decodeStream(fis)
        }

        fun getPreviewImageBitmap(homeGalleryImage : HomeGalleryImage) : Bitmap {
            val fis = FileInputStream(homeGalleryImage.previewContent)
            return BitmapFactory.decodeStream(fis)
        }
    }
}