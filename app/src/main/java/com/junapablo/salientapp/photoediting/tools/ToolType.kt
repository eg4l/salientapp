package com.junapablo.salientapp.photoediting.tools

import com.junapablo.salientapp.R


enum class ToolType(val toolName: String, val icon: Int) {
    BG_GALLERY("Background \nfrom gallery", R.drawable.ic_gallery),
    BG_PHOTO("Background \nfrom photo", R.drawable.ic_camera),
    BRUSH("Brush", R.drawable.ic_brush),
    ERASER("Eraser", R.drawable.ic_eraser),
    STICKER("Cutout", R.drawable.ic_sticker),
    UNDO("Undo", R.drawable.ic_undo),
    REDO("Redo", R.drawable.ic_redo),
    SAVE("Save", R.drawable.ic_save),
}