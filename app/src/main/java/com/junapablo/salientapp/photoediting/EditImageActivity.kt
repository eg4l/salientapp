package com.junapablo.salientapp.photoediting

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.junapablo.salientapp.R
import com.junapablo.salientapp.photoediting.stickers.StickersBottomSheetDialogFragment
import com.junapablo.salientapp.photoediting.stickers.StickersBottomSheetDialogFragment.StickerListener
import com.junapablo.salientapp.photoediting.tools.ToolType
import com.junapablo.salientapp.photoediting.tools.ToolbarItemAdapter
import com.junapablo.salientapp.storage.StorageManager
import ja.burhanrashid52.photoeditor.PhotoEditor
import ja.burhanrashid52.photoeditor.PhotoEditorView
import java.io.IOException


class EditImageActivity : AppCompatActivity(),
    StickerListener, ToolbarItemAdapter.OnSelectedListener {
    private lateinit var photoEditor: PhotoEditor
    private lateinit var photoEditorView: PhotoEditorView
    private lateinit var stickerBSFragment: StickersBottomSheetDialogFragment
    private val toolbarItemsAdapter = ToolbarItemAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_image)

        photoEditorView = findViewById(R.id.photoEditorView)
        stickerBSFragment = StickersBottomSheetDialogFragment(this)

        findViewById<RecyclerView>(R.id.rvConstraintTools).also {
            it.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            it.adapter = toolbarItemsAdapter
        }

        findViewById<Button>(R.id.buttonPickColor).setOnClickListener {
            pickBrushColor()
        }

        findViewById<SeekBar>(R.id.seekBrushSize).setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean) {}

            override fun onStartTrackingTouch(seek: SeekBar) {}

            override fun onStopTrackingTouch(seek: SeekBar) {
                photoEditor.brushSize = seek.progress.toFloat()
            }
        })

        photoEditor = PhotoEditor.Builder(this, photoEditorView)
            .setPinchTextScalable(true)
            .build()
        handleIntentImage(photoEditorView.source)
    }

    private fun handleIntentImage(source: ImageView) {
        if (intent != null) {
            if (intent.action == Intent.ACTION_EDIT) {
                try {
                    val bitmapPath = intent.getStringExtra(BITMAP_INPUT_EXTRA_PATH)
                    val bitmap = StorageManager.loadBitmapFromFilename(bitmapPath!!)
                    photoEditor.addImage(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                val intentType = intent.type
                if (intentType != null && intentType.startsWith("image/")) {
                    val imageUri = intent.data
                    if (imageUri != null) {
                        source.setImageURI(imageUri)
                    }
                }
            }
        }
    }


    private fun captureView(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    private fun saveImage() {
        val bitmap = captureView(findViewById(R.id.photoEditorView))
        if (bitmap != null) {
            StorageManager.saveImageInEditGallery(bitmap)
        }
        Toast.makeText(this, "Edited photo was saved to your gallery!", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                CAMERA_REQUEST -> {
                    val photo = data!!.extras!!["data"] as Bitmap?
                    photoEditorView.source.setImageBitmap(photo)
                }
                PICK_REQUEST -> try {
                    val uri = data!!.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                    photoEditorView.source.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onStickerClick(bitmap: Bitmap) {
        photoEditor.addImage(bitmap)
    }

    private fun showBottomSheetDialogFragment(fragment: BottomSheetDialogFragment?) {
        if (fragment == null || fragment.isAdded) {
            return
        }
        fragment.show(supportFragmentManager, fragment.tag)
    }

    override fun onToolbarItemSelected(toolType: ToolType) {
        when (toolType) {
            ToolType.BRUSH -> {
                Toast.makeText(this, "Selected brush", Toast.LENGTH_SHORT).show()
                photoEditor.setBrushDrawingMode(true)
            }
            ToolType.ERASER -> {
                Toast.makeText(this, "Selected eraser", Toast.LENGTH_SHORT).show()
                photoEditor.brushEraser()
            }
            ToolType.STICKER -> showBottomSheetDialogFragment(stickerBSFragment)
            ToolType.BG_PHOTO -> {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
            ToolType.BG_GALLERY -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST)
            }
            ToolType.UNDO -> photoEditor.undo()
            ToolType.REDO -> photoEditor.redo()
            ToolType.SAVE -> saveImage()
        }
    }

    fun pickBrushColor() {
        ColorPickerDialogBuilder
            .with(this)
            .setTitle("Choose color")
            .initialColor(Color.WHITE)
            .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
            .lightnessSliderOnly()
            .setPositiveButton(
                "ok"
            ) { dialog, selectedColor, allColors -> photoEditor.brushColor = selectedColor }
            .build()
            .show()
    }

    companion object {
        private const val CAMERA_REQUEST = 6401
        private const val PICK_REQUEST = 6402
        const val BITMAP_INPUT_EXTRA_PATH = "bitmap_input_extra"
    }
}