package com.junapablo.salientapp.photoediting.tools

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.junapablo.salientapp.R

class ToolbarItemAdapter(val onSelectedListener: OnSelectedListener) :
    RecyclerView.Adapter<ToolbarItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ToolbarItemAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_editing_tools, viewGroup, false)
        return ToolbarItemAdapter.ViewHolder(onSelectedListener, view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.toolIcon.setImageResource(ToolType.values()[position].icon)
        holder.toolDescription.text = ToolType.values()[position].toolName
    }

    override fun getItemCount(): Int {
        return ToolType.values().size
    }


    interface OnSelectedListener {
        fun onToolbarItemSelected(toolType: ToolType)
    }

    class ViewHolder(onSelectedListener: OnSelectedListener, itemView: View) :
        RecyclerView.ViewHolder(
            itemView
        ) {
        val toolIcon: ImageView = itemView.findViewById(R.id.toolIcon)
        val toolDescription: TextView = itemView.findViewById(R.id.toolDescription)

        init {
            toolIcon.setOnClickListener {
                onSelectedListener.onToolbarItemSelected(ToolType.values()[layoutPosition])
            }
        }
    }
}