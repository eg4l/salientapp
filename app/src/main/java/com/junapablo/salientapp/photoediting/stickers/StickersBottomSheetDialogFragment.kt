package com.junapablo.salientapp.photoediting.stickers

import android.app.Dialog
import android.graphics.Bitmap
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.junapablo.salientapp.R

class StickersBottomSheetDialogFragment(val stickerListener: StickerListener) :
    BottomSheetDialogFragment() {
    interface StickerListener {
        fun onStickerClick(bitmap: Bitmap)
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        val contentView = View.inflate(context, R.layout.fragment_bottom_sticker_dialog, null)
        dialog.setContentView(contentView)
        val rvStickers = contentView.findViewById<RecyclerView>(R.id.rvStickers)
        rvStickers.layoutManager = GridLayoutManager(activity, 3)
        rvStickers.adapter = StickersAdapter(this).also {
            it.refreshStickers()
        }
    }
}