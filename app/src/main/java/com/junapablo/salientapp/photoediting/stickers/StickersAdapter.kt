package com.junapablo.salientapp.photoediting.stickers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.junapablo.salientapp.R
import com.junapablo.salientapp.storage.StorageManager
import java.io.File


class StickersAdapter(val parent: StickersBottomSheetDialogFragment) :
    RecyclerView.Adapter<StickersAdapter.ViewHolder>() {
    val stickers = mutableListOf<File>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_sticker, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imgSticker.setImageBitmap(StorageManager.loadBitmap(stickers[position]))
    }

    override fun getItemCount(): Int {
        return stickers.size
    }

    fun refreshStickers() {
        stickers.clear()
        StorageManager.getMainGalleryFiles().forEach { (_, previewFile) ->
            stickers.add(previewFile)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgSticker: ImageView = itemView.findViewById(R.id.imgSticker)

        init {
            itemView.setOnClickListener {
                parent.stickerListener.onStickerClick(StorageManager.loadBitmap(stickers[layoutPosition])!!)
                parent.dismiss()
            }
        }
    }
}