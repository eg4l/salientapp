package com.junapablo.salientapp.storage

class BitmapSaveFailure(s: String) : Exception(s)
class FileNotDeleted(s: String) : Exception(s)