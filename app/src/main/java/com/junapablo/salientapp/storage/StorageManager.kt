package com.junapablo.salientapp.storage

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.junapablo.salientapp.R
import com.junapablo.salientapp.homeactivity.image.HomeGalleryImage
import java.io.File
import java.io.IOException

private enum class Storage(val key: String) {
    MAIN("spMAIN"),
    EDIT("spEDIT")
}

private class TimeStamp() {
    private val ts = System.currentTimeMillis().toString()
    fun get(): String {
        return this.ts
    }
}

// TODO: Transactional add and delete to local database, current implementation is not optimal (if needed at all)
//  possibilities - indexing by same id as in List or by fileName String
//  currently we have to store Strings because HashSet doesn't have to maintain order on add or delete
object StorageManager {
    private lateinit var appContext: Context

    private lateinit var storageDir: File
    private lateinit var mainGalleryDirs: Array<File>
    private lateinit var editGalleryDir: File

    private lateinit var sp: SharedPreferences

    /**
     *  Setup StorageManager and feed with given context
     *
     *  @param: c: Context - feed with MainActivity context to avoid memory leaks
     */
    fun setup(c: Context) {
        this.appContext = c
        this.storageDir = c.filesDir
        this.mainGalleryDirs = arrayOf(
                File(this.storageDir.path, "raw"),
                File(this.storageDir.path, "preview")
        )
        this.mainGalleryDirs.forEach {
            if (!it.exists()) {
                it.mkdir()
            }
        }

        this.editGalleryDir = File(this.storageDir.path, "edit").apply {
            if (!this.exists()) {
                this.mkdir()
            }
        }

        this.sp = this.appContext.getSharedPreferences(
                "IMG_STORAGE",
                Context.MODE_PRIVATE
        )
    }

    private fun spAddFile(storage: Storage, value: String) {
        val existingValues = HashSet<String>(this.sp.getStringSet(storage.key, HashSet<String>()))
        val editor = this.sp.edit()
        existingValues.add(value)
        editor.clear()
        editor.putStringSet(storage.key, existingValues)
        editor.apply()
    }

    private fun spDeleteFile(storage: Storage, value: String) {
        val existingValues = HashSet<String>(this.sp.getStringSet(storage.key, HashSet<String>()))
        val editor = this.sp.edit()
        existingValues.remove(value)
        editor.clear()
        editor.putStringSet(storage.key, existingValues)
        editor.apply()
    }

    /**
     *  Save raw + preview in MainGallery
     *  Stores new photo in MainGallery storage directory
     *
     *  @param: bitmaps of raw + preview
     *  @throws: throws BitmapSaveFailure on failure
     */
    fun saveImagesInMainGallery(imgRaw: Bitmap, imgPreview: Bitmap): HomeGalleryImage {
        val ts = TimeStamp()
        val fileName = "${ts.get()}.png"

        val sourceFile = saveBitmap(imgRaw, this.mainGalleryDirs[0], fileName)
        val previewFile = saveBitmap(imgPreview, this.mainGalleryDirs[1], fileName)

        this.spAddFile(Storage.MAIN, fileName)

        return HomeGalleryImage(sourceFile, previewFile, fileName)
    }

    /**
     *  Delete image with given imageName from storage
     */
    fun deleteImageFromMainGallery(imageName: String) {
        this.spDeleteFile(Storage.MAIN, imageName)

        for (dir in this.mainGalleryDirs) {
            try {
                this.deleteFile(File(dir, imageName))
            } catch (e: FileNotDeleted) {
                e.printStackTrace()
            }
        }
    }

    /**
     *  Get all Files from MainGallery
     *
     *  @return: Non-mutable List of Pair<File, File> where first is raw and second is preview
     */
    fun getMainGalleryFiles(): List<Pair<File, File>> {
        val existingValues = this.sp.getStringSet(Storage.MAIN.key, HashSet<String>())!!
        val fileList: MutableList<Pair<File, File>> = mutableListOf()
        for (s in existingValues) {
            fileList.add(
                    Pair(
                            File(this.mainGalleryDirs[0], s),
                            File(this.mainGalleryDirs[1], s)
                    )
            )
        }

        return fileList.toList()
    }

    /**
     *  Save edit in EditGallery
     *  Stores new photo in EditGallery storage directory
     *
     *  @param: bitmap of edited img
     *  @throws: throws BitmapSaveFailure on failure
     */
    fun saveImageInEditGallery(img: Bitmap): String {
        val ts = TimeStamp()
        val fileName = "${ts.get()}.png"

        saveBitmap(img, this.editGalleryDir, fileName)
        this.spAddFile(Storage.EDIT, fileName)

        return fileName
    }

    /**
     *  Delete image with given imageName from storage
     */
    fun deleteImageFromEditGallery(imageName: String) {
        this.spDeleteFile(Storage.EDIT, imageName)

        try {
            this.deleteFile(File(this.editGalleryDir, imageName))
        } catch (e: FileNotDeleted) {
            e.printStackTrace()
        }
    }

    /**
     *  Get all Files from EditGallery
     *
     *  @return: Non-mutable List of Files from EditGallery
     */
    fun getEditGalleryFiles(): List<File> {
        val existingValues = this.sp.getStringSet(Storage.EDIT.key, HashSet<String>())!!
        val fileList: MutableList<File> = mutableListOf()
        for (s in existingValues) {
            fileList.add(
                    File(this.editGalleryDir, s)
            )
        }

        return fileList.toList()
    }

    /**
     *  Load bitmap from file
     *
     *  @return: Bitmap? - null on failure
     */
    fun loadBitmap(file: File): Bitmap? {
        val bitmapFactory: Bitmap? = BitmapFactory.decodeFile(file.path)
        if (bitmapFactory != null) {
            return Bitmap.createScaledBitmap(
                    bitmapFactory,
                    this.appContext.resources.getInteger(R.integer.image_width),
                    this.appContext.resources.getInteger(R.integer.image_height),
                    false
            )
        }

        return null
    }

    //NOTE: delete this and replace with something better ASAP
    fun loadBitmapFromFilename(filename: String, isRaw: Boolean = false): Bitmap? {
        val file = File(if(isRaw) mainGalleryDirs[0] else mainGalleryDirs[1], filename)
        return loadBitmap(file)
    }

    private fun saveBitmap(img: Bitmap, destDir: File, fileName: String) : File {
        val file = File(destDir, fileName)
        if (file.exists()) {
            throw BitmapSaveFailure("File exists!")
        }

        try {
            val out = file.outputStream()
            img.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.close()
        } catch (e: IOException) {
            e.printStackTrace()
            throw BitmapSaveFailure("Unable to save bitmap because of IO failure!")
        }
        return file
    }

    private fun deleteFile(file: File) {
        if (!file.delete()) {
            throw FileNotDeleted("File not deleted!")
        }
    }
}